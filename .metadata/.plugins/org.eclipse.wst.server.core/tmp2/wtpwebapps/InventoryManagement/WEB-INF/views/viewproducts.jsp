<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inventory Management - Products View</title>
<style>
table{
border-collapse:collapse;
width:100%
}
th,td
{
	padding:10px;
	text-align:left;
	border-bottom:1px solid #DDD
}
tr:hover{
	background-color:#7cd0da;
}
</style>
</head>
<body>
<h2 style="text-align:center;">Available Products</h2>
<table>
<tr>
<th>Product ID</th>
<th>Product Name</th>
<th>Product Description</th>
<th>Product Quantity</th>
<th>Product Price</th>
</tr>
<c:forEach var="prdcts" items="${userprodlist}">
<tr>
<td>${prdcts.id}</td>
<td>${prdcts.prodname}</td>
<td>${prdcts.proddesc}</td>
<td>${prdcts.prodqty}</td>
<td>${prdcts.prodrice}</td>
<tr>
</c:forEach>
</table>
</body>
</html>