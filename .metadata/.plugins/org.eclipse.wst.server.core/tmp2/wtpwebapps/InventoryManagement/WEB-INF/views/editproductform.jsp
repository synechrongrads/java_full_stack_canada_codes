<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inventory Management - New Product Entry</title>
</head>
<body>
<form:form method="POST" action="/InventoryManagement/updateprocess">
<table>
	<tr>
	<td>Product Id</td>
	<td><form:input path="id"/></td>
	</tr>
	<tr>
	<td>Product Name</td>
	<td><form:input path="prodname"/></td>
	</tr>
	<tr>
	<td>Product Description</td>
	<td><form:input path="proddesc"/></td>
	</tr>
	<tr>
	<td>Product Quantity</td>
	<td><form:input path="prodqty"/></td>
	</tr>
	<tr>
	<td>Product Price</td>
	<td><form:input path="prodrice"/></td>
	</tr>
	<tr>
	<td colspan="2"><input type="submit" value="Update Products"></td>
	</tr>
</table>
</form:form>
</body>
</html>