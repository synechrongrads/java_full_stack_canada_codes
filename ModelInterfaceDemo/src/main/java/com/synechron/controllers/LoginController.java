package com.synechron.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class LoginController {

	@RequestMapping("/login")
	public String display(@RequestParam("name")String name, @RequestParam("pwd")String password, Model m) {
		if(name.equals("admin") && password.equals("admin")) {
			String msg  = "Welcome! " + name;
			m.addAttribute("message",msg);
			return "admindashboard";
		}
		else {
			String msg = "Invalid username or password";
			m.addAttribute("errormsg",msg);
			return "exception";
		}
	}
}
