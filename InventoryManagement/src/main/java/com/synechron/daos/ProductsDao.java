package com.synechron.daos;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.synechron.models.Products;

public class ProductsDao {
	JdbcTemplate prdtemplate;
	public void setTemplate(JdbcTemplate template) {
		this.prdtemplate = template;
	}
	
	//Save new Product
	public int saveProducts(Products p) {
		String create_product = "insert into products (id,prodname,proddesc,prodqty,prodrice)"+
					"values("+p.getId()+",'"+p.getProdname()+"','"+p.getProddesc()+"',"+
				p.getProdqty()+","+p.getProdrice()+");";
		return prdtemplate.update(create_product);
	}
	//Retrieve all the products
	public List<Products> getAllProducts(){
		String get_all = "select * from Products;";
		return prdtemplate.query(get_all, new BeanPropertyRowMapper<Products>(Products.class));
	}
	//Retrieve a specific product based on product id
	public Products getProductsById(int id){
		String get_by_id= "select * from Products where id=?;";
		return prdtemplate.queryForObject(get_by_id, new BeanPropertyRowMapper<Products>(Products.class),id);
	}
	//Update a specific product
	
	public int updateProduct(Products p) {
		String updateProduct = "Update Products set prodname=?,proddesc=?,prodqty=?,prodrice=? where id=?";
		return prdtemplate.update(updateProduct, new Object[] {
												p.getProdname(),
												p.getProddesc(),
												p.getProdqty(),
												p.getProdrice(),
												p.getId()
		});
	} 
	//Delete a specific product
	public int deleteProduct(int id) {
		String deleteproduct = "delete from Products where id=?";
		return prdtemplate.update(deleteproduct,id);
	}
}
