package com.synechron.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.synechron.daos.ProductsDao;
import com.synechron.models.Products;

@Controller
public class ProductController {
	@Autowired
	ProductsDao dao;
	//login process:
	@RequestMapping("/loginprocess")
	public String loginpricess(@RequestParam("username")String username, @RequestParam("password")String password,Model m) {
		//username is admin -> create products
		if(username.equals("admin")&&password.equals("admin")) {
			//redirect the admin to createproduct form
			return "redirect:/productform";
		}
		else {
			//username is not admin -> viewproducts
			return "redirect:/viewproducts";
		}
	}
	@RequestMapping("/productform")
	public String productEntry(Model m) {
		m.addAttribute("command",new Products());
		return "productform";
	}
	
	@RequestMapping(value="/saveproducts",method = RequestMethod.POST)
	public String saveProducts(@ModelAttribute("prd")Products prd) {
		dao.saveProducts(prd);
		return "redirect:/productlist";
	}
	
	@RequestMapping("/viewproducts")
	public String viewProducts(Model m) {
		List<Products> prdlist = dao.getAllProducts();
		m.addAttribute("userprodlist",prdlist);
		return "viewproducts";
	}
	
	@RequestMapping("/productlist")
	public String productList(Model m) {
		List<Products> prodlist = dao.getAllProducts();
		m.addAttribute("prodlist",prodlist);
		return "productlist";
	}
	
	@RequestMapping(value="/updateproduct/{id}")
	public String updateProductEntry(@PathVariable int id, Model m) {
		Products p = dao.getProductsById(id);
		m.addAttribute("command",p);
		return "editproductform";
	}
	
	@RequestMapping(value="/updateprocess", method=RequestMethod.POST)
	public String updateProducts(@ModelAttribute("prod") Products prod) {
		dao.updateProduct(prod);
		return "redirect:/productlist";
	}
	
	@RequestMapping(value="/deleteproduct/{id}", method=RequestMethod.GET)
	public String deleteProducts(@PathVariable int id) {
		dao.deleteProduct(id);
		return "redirect:/productlist";
	}
}

