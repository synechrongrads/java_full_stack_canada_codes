package com.synechron.models;

public class Products {
private int id;
private String prodname;
private String proddesc;
private int prodqty;
private float prodrice;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getProdname() {
	return prodname;
}
public void setProdname(String prodname) {
	this.prodname = prodname;
}
public String getProddesc() {
	return proddesc;
}
public void setProddesc(String proddesc) {
	this.proddesc = proddesc;
}
public int getProdqty() {
	return prodqty;
}
public void setProdqty(int prodqty) {
	this.prodqty = prodqty;
}
public float getProdrice() {
	return prodrice;
}
public void setProdrice(float prodrice) {
	this.prodrice = prodrice;
}


}
